import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var config = {
  apiKey: "AIzaSyCcCzyDOAT3PbjhPcSg4Igy8DT3qZdj4_Q",
  authDomain: "reactapp-666666.firebaseapp.com",
  databaseURL: "https://reactapp-666666.firebaseio.com",
  projectId: "reactapp-666666",
  storageBucket: "reactapp-666666.appspot.com",
  messagingSenderId: "105953563420"
};

firebase.initializeApp(config);

firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase
