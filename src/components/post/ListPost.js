import React, { Component } from 'react'
import PostCard from './postCard/PostCard'
import { connect } from 'react-redux'

class ListPost extends Component {
  render() {
    const { posts } = this.props
    return (
      <div className="list-post">
        <PostCard />
      </div>
    )
  }
}

const mapState = (state) => {
  return { posts: state.post.posts }
}

export default connect(mapState)(ListPost)