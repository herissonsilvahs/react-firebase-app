import auth from './reducers/auth'
import post from './reducers/post'

import { combineReducers } from 'redux'

const index = combineReducers({
  auth,
  post
})

export default index